#!/bin/bash

if [ ! -d resultados ];
then
	mkdir resultados
fi


for file in "$@"
do
	for i in {3,5}
	do
		instance="$(basename "$file")"
		instance="${instance%.*}"
		resultsfile="resultados/$instance"_"$i"_results.txt
		./Debug/P4Metaheuristics $file $i > "$resultsfile"

		output="resultados/$instance"_"$i".png
		instanceName="$(sed 's/_/ /g' <<< $instance)"

cat << _end_ | gnuplot
	set terminal png
	set output "$output"
	set key right bottom
	set title "$instanceName con $i mochilas"
	set xlabel "Numero de ejecuciones"
	set ylabel "Fitness"
	set logscale x	
	plot '$resultsfile' using 1 with points ps 0.2 title 'Current GA', '$resultsfile' using 2 with lines title 'Best GA', '$resultsfile' using 3 with points ps 0.2 title 'Current ACO', '$resultsfile' using 4 with lines title 'Best ACO'
_end_

	echo -e "Generado: $output"

	done


done
#set logscale x
#'$resultsfile' using 1 with points ps 0.2 title 'Current GA', '$resultsfile' using 2 with lines title 'Best GA', 
#, '$resultsfile' using 3 with lines title 'Current ACO', '$resultsfile' using 4 with lines title 'Best ACO'

#echo -e "Graphic generated: $2"